import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'App',
    redirect: 'Index',
    component: () => import('../views/index/index.vue')
  },
  {
    path: '/Index',
    name: 'Index',
    redirect: '',
    component: () => import('../views/index/index.vue')
  },
  {
    path: '/Default',
    name: 'Default',
    component: () => import('../views/default/index.vue')
  },
  {
    path: '/Async',
    name: 'Async',
    component: () => import('../views/async/index.vue')
  },
  {
    path: '/Plugin',
    name: 'Plugin',
    component: () => import('../views/plugin/index.vue')
  },
  {
    path: '/SourceCode',
    name: 'SourceCode',
    component: () => import('../views/sourceCode/index.vue')
  },
  {
    path: '/Link',
    name: 'Link',
    component: () => import('../views/link/index.vue')
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
